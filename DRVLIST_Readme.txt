The Driver List has the following construction:
- One driver requires 5 lines, each being:
-- Display name of the driver
-- JAR file name (without the extension '.jar') in the 'res/drivers' directory
-- Class name to load (eg. for the Oracle driver it is 'oracle.jdbc.driver.OracleDriver')
-- Connection string where the host to be replaced is '#HOST#' without apostrophes (without the 'jdbc:' prefix). For example this is 'oracle:thin:@#HOST#' in the case of the Oracle driver.
-- Tooltip text for the host input field. For example, the Oracle driver requires the "host:port:sid" format, this will be displayed in a tooltip if the current active driver is the Oracle driver. Leave this row empty if you don't want to show a tooltip for this field.

NOTE: if you add drivers to the end of the file, please add a final newline (\n) to it or the last driver won't be read and loaded.