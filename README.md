# Universal Database Manager -- Readme #

## What is it? ##
This is a Java(FX) application that was brought up as an optional task on one of the courses I attend. The concept is to make a Java application that is able to perform database operations regardless of the engine behind it. This takes advantage of JDBC's strong point that unifies database management. This project is an implementation of that concept with JavaFX behind it as it provides a far richer GUI kit than Swing does.

Currently, the project ships with 5 JDBC drivers:

* MySQL
* Microsoft SQL Server
* Oracle (Thin/JDBC)
* PostgreSQL
* SQLite

The app loads the drivers dynamically using information from the DRVLIST.txt file that is stored in the 'res/drivers' folder. For the structure of this file, please see 'DRVLIST_Readme.txt' in the root folder. This dynamic JAR loading is achieved through the heavy use of reflection, which also means that the app doesn't need any external JARs to load, though it won't work without drivers, since those are the libraries that provide the necessary functionality to manage databases.

The application has the following features:

* A resizeable, overseeable, good looking UI with custom CSS adjustments to complement the layout of the application
* Ability to perform database operations in the background, so the UI stays responsive even during long queries
* Ability to cancel ongoing queries
* Database Information window that extracts lots of information from the current JDBC driver and the database
* History view that stores past (successful) queries and allows them to be copied back into the editing area or executed again right from the context menu
* Dynamic detection and loading of drivers

## WARNING ##
This is a strictly home-made (*"for fun"*) project and is not meant to be distributed anywhere else besides this Bitbucket repository or sold, licenced in any ways.

## SCREENSHOTS ##

![UDM_SS01.PNG](https://bitbucket.org/repo/baq97r5/images/3100575591-UDM_SS01.PNG)

![UDM_SS02.PNG](https://bitbucket.org/repo/baq97r5/images/1878433972-UDM_SS02.PNG)

## TODO ##

* Construct an icon for the application
* Add option to limit maximum entries from queries
* Add better support for feedback from CREATE/DROP commands