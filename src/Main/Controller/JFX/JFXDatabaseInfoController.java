package Main.Controller.JFX;


import Main.Controller.UniversalController;
import Main.Model.DatabaseAttribute;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

import java.lang.reflect.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/** This is the controller class behind the Database Information window. It handles getting the appropriate values
 * using reflection and provides code-behind for UI interaction.
 * @author Imre Gera */
public class JFXDatabaseInfoController {
    // The TableView containing information about the database
    @FXML private TableView c_tDatabaseInfo;

    // The list that contains all the data supplied to c_tDatabaseInfo
    private ObservableList<DatabaseAttribute> dbMeta;

    /** Runs when the Database Information window is opened. Fills the main table. */
    public void initialize() {
        /* Set up the TableView */
        TableColumn tc_AttrName = new TableColumn("Attribute");
        tc_AttrName.setCellValueFactory(new PropertyValueFactory<DatabaseAttribute, String>("attributeName"));
        tc_AttrName.setSortType(TableColumn.SortType.ASCENDING);

        TableColumn tc_AttrValue = new TableColumn("Value");
        tc_AttrValue.setCellValueFactory(new PropertyValueFactory<DatabaseAttribute, String>("attributeValue"));

        c_tDatabaseInfo.getItems().clear();
        c_tDatabaseInfo.getColumns().clear();
        c_tDatabaseInfo.getColumns().addAll(tc_AttrName, tc_AttrValue);

        c_tDatabaseInfo.getSortOrder().add(tc_AttrName);

        dbMeta = getDatabaseMetaData();

        c_tDatabaseInfo.setItems(dbMeta);

    }

    private ObservableList<DatabaseAttribute> getDatabaseMetaData() {
        ArrayList<DatabaseAttribute> dbMeta_arrayList = new ArrayList<DatabaseAttribute>();

        try(Connection conn = DriverManager.getConnection(UniversalController.DB_CONNSTR, UniversalController.DB_CONNUSR, UniversalController.DB_CONNPW)) {
            DatabaseMetaData dbmd = conn.getMetaData();

            // List of available methods in the 'DatabaseMetaData' class
            Method[] methods = dbmd.getClass().getMethods();

            // Accepted method return types
            ArrayList<Class> returnTypes = new ArrayList<Class>();
            returnTypes.add(int.class);
            returnTypes.add(String.class);
            returnTypes.add(boolean.class);
            returnTypes.add(long.class);

            // Fine use of reflection :)
            for(Method m : methods) {
                // If a method's return type is appropriate, requires 0 parameters and is not abstract:
                if(returnTypes.contains(m.getReturnType())
                        && m.getParameterCount() == 0
                        && !Modifier.isAbstract(m.getModifiers())) {
                    try {
                        Object result = m.invoke(dbmd);
                        if(result != null) {
                            DatabaseAttribute dba = new DatabaseAttribute();
                            dba.setAttributeName(m.getName().replace("get", ""));
                            dba.setAttributeValue(result.toString());
                            dbMeta_arrayList.add(dba);
                        }
                    } catch(IllegalAccessException | InvocationTargetException exc) {
                        System.err.println("# DBINFO: Whoops, something went wrong during reflection.");
                        exc.printStackTrace();
                    }
                }
            }

            dbMeta = FXCollections.observableList(dbMeta_arrayList);
        } catch(SQLException e) {
            e.printStackTrace(UniversalController.exception_pw);
            UniversalController.showMessageDialog(
                    Alert.AlertType.ERROR,
                    "Error",
                    "Failed to retrieve database metadata, possibly due to an SQL error.",
                    "Here is the exception stacktrace:",
                    UniversalController.exception_sw.toString()
            );
            //e.printStackTrace();
        }

        return FXCollections.observableList(dbMeta_arrayList);
    }

}