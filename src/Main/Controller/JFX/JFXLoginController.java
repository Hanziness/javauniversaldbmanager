package Main.Controller.JFX;

import Main.Controller.UniversalController;
import com.sun.org.apache.xpath.internal.operations.Bool;
import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import java.lang.management.PlatformLoggingMXBean;
import java.util.ArrayList;

/** The controller class behind the login window. Handles user interactions.
 * @author Imre Gera */
public class JFXLoginController {

        /* Stores the list of available driver names, for use in the ChoiceBox. */
        private ObservableList<String> driverList;

        /* FXML Components */
        @FXML
        private Button bLogin;
        @FXML
        private TextField tHostName;
        @FXML
        private TextField tUserName;
        @FXML
        private PasswordField tPassword;
        @FXML
        private ProgressIndicator pLoginProgress;
        @FXML
        private ChoiceBox<String> cConnectionPlugin;
        @FXML
        private Label lConnectionStatus;
        @FXML
        private Tooltip tooltip_Host;

        /* Used for storing the exception that occurred during login. */
        public static Exception loginException;

        /* The current value of tooltip_Host's tooltip text. */
        private String currentTooltip;

        /** Runs when the JavaFX window is initialized. Sets up the available drivers and registers extra UI events. */
        @FXML
        public void initialize() {
            driverList = FXCollections.observableArrayList(UniversalController.getAvailableDriverNames());

            // Check if no drivers were loaded.
            if(driverList.size() < 1) {
                lockControls(true); // lock controls, since the application won't function

                UniversalController.showMessageDialog(Alert.AlertType.WARNING,
                        "",
                        "Failed to load any drivers. The application won't be able to connect to any database." +
                                "Please check if 'res/drivers/DRVLIST.txt' is properly formatted and that the directory " +
                                "contains the drivers you want to use.",
                        "",
                        "");
            }

            cConnectionPlugin.setItems(driverList);

            /* Changes the tooltip text when the driver model is changed */
            cConnectionPlugin.getSelectionModel()
                    .selectedIndexProperty()
                    .addListener(
                            new ChangeListener<Number>() {
                                @Override
                                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                                    currentTooltip = UniversalController.driverPool.get((int)newValue).getTooltip();
                                    if(currentTooltip.equals(""))
                                        tHostName.setTooltip(null);
                                    else {
                                        tooltip_Host.setText(currentTooltip);
                                        tHostName.setTooltip(tooltip_Host);
                                    }
                                }
                            }
                    );
            cConnectionPlugin.setValue(driverList.get(0)); // Sets the default value to the first item
        }

        /** Performs UI actions when the login button is clicked. */
        @FXML
        protected void loginClicked(ActionEvent e) {

            updateConnectionStatus("Initializing...");
            loginException = null;

            // Lock access to the UI while a login is in progress
            lockControls(true);

            // Set the ProgressIndicator to spin
            pLoginProgress.setProgress(-1);

            FadeTransition progressFade = new FadeTransition(Duration.millis(500), pLoginProgress);
            progressFade.setFromValue(0);
            progressFade.setToValue(1);
            progressFade.play();

            updateConnectionStatus("Performing login...");


            // Background task for handling database operations so that the GUI won't freeze while it is still in progress.
            Service<Boolean> loginService = new Service<Boolean>() {
                @Override
                protected Task<Boolean> createTask() {
                    return new Task<Boolean>() {
                        @Override
                        protected Boolean call() throws Exception {
                            boolean success = UniversalController.performLogin(UniversalController.getAvailableDriverNames().indexOf(cConnectionPlugin.getValue()),
                                    tHostName.getText(),
                                    tUserName.getText(),
                                    tPassword.getText());
                            Platform.runLater(() -> {
                                lockControls(false);
                            });
                            return success;
                        }
                    };
                }

            };

            /* When the task finishes, check if login was successful.
            * If login succeded, then open the main window, otherwise throw an error message and unlock the UI */
            loginService.setOnSucceeded(event -> {
                boolean success = loginService.getValue();
                if(success) {
                    updateConnectionStatus("Successful login!");

                    UniversalController.showMessageDialog(Alert.AlertType.INFORMATION,
                            null,
                            "Successful login",
                            "",
                            "");

                    openMainWindow();
                    ((Node)(e.getSource())).getScene().getWindow().hide();  // closes this window
                } else {
                    updateConnectionStatus("Login failed!");

                    String excHeader = "";
                    String excMessage = "";

                    if(loginException != null) {
                        excHeader = "Here is the stacktrace of the issue:";
                        loginException.printStackTrace(UniversalController.exception_pw);
                        excMessage = UniversalController.exception_sw.toString();
                    }


                    UniversalController.showMessageDialog(Alert.AlertType.ERROR,
                            null,
                            "Login failed!",
                            excHeader,
                            excMessage);

                    Platform.runLater(() -> {
                        progressFade.setFromValue(1);
                        progressFade.setToValue(0);
                        progressFade.play();
                    });
                }
            });

            // Starts the login process
            loginService.restart();
        }

        /** Tries to open the main window that allows performing actions on the database. */
        private void openMainWindow() {
            Parent root;
            try {
                root = FXMLLoader.load(getClass().getClassLoader().getResource("Main/GUI/JFX/MainWindow.fxml"));
                Stage st = new Stage();
                st.setTitle("Universal Database Manager");
                st.setScene(new Scene(root, 800, 600));
                st.show();

            } catch(Exception e) {
                e.printStackTrace(UniversalController.exception_pw);
                UniversalController.showMessageDialog(
                        Alert.AlertType.ERROR,
                        "Error",
                        "Failed to open main window.",
                        "Here is the exception stacktrace:",
                        UniversalController.exception_sw.toString()
                );
            }
        }

        /** Updates the connection status label
         * @param newStatus The new text to be shown as the connection status. */
        @FXML private void updateConnectionStatus(String newStatus) {
            lConnectionStatus.setText(newStatus);
        }

        /** Locks or unlocks access the UI of this window.
         * @param lock Whether to lock (true) or unlock (false) the UI elements of the window. */
        private void lockControls(boolean lock) {
            tHostName.setDisable(lock);
            tUserName.setDisable(lock);
            tPassword.setDisable(lock);
            cConnectionPlugin.setDisable(lock);
            bLogin.setDisable(lock);
        }
}
