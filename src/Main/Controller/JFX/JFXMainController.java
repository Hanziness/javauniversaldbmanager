package Main.Controller.JFX;

import Main.Controller.UniversalController;
import Main.Model.DriverModel;
import com.sun.javafx.tk.Toolkit;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import sun.applet.Main;

import javax.swing.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.*;
import java.util.concurrent.FutureTask;

import static Main.Controller.UniversalController.showMessageDialog;

public class JFXMainController {

    @FXML private Button c_bPerform;
    @FXML private Button c_bCancel;

    @FXML private TextArea c_tInput;

    @FXML private TableView<ObservableList<String>> c_tOutputTable;

    @FXML private TextArea c_tOutputLogs;

    @FXML private ListView<String> c_lHistory;

    @FXML private TabPane tp_Main;
    private SingleSelectionModel<Tab> tp_Main_selectionModel;

    @FXML private ProgressIndicator pi_Working;

    @FXML private MenuItem menu_Close;
    @FXML private MenuItem menu_Disconnect;
    @FXML private MenuItem menu_About;
    @FXML private MenuItem menu_DatabaseInformation;

    @FXML private ContextMenu c_lHistoryContextMenu;
    @FXML private MenuItem c_lHistoryContext_Execute;
    @FXML private MenuItem c_lHistoryContext_CopyOver;
    @FXML private MenuItem c_lHistoryContext_ClearSelection;

    private Service<Void> SQLService;


    /** Runs when the window is opened. Sets the value of tp_Main_selectionModel so that other methods can access it. */
    @FXML public void initialize() {
        tp_Main_selectionModel = tp_Main.getSelectionModel();

        // History view: set text when item clicked:
        /*
        c_lHistory.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue != null) {
                c_tInput.setText(newValue);
            }
        });
        */
    }

    /** Runs when the 'Perform' button is clicked. Invokes the performDatabaseAction method to run the entered query. */
    @FXML protected void performButton_Click(ActionEvent e) {
        String inputQuery = c_tInput.getText().trim();
        if(inputQuery.endsWith(";")) {
            inputQuery = inputQuery.substring(0, inputQuery.length()-1); // remove ';' from the string
        }

        performDatabaseAction(inputQuery);
    }

    @FXML protected void cancelButton_Click(ActionEvent e) {
        if(SQLService.isRunning()) {
            SQLService.cancel();
        }
    }

    /** Performs an SQL Query in the background while maintaining UI responsibility
     * @param inputQuery A query string, be it SELECT, INSERT, UPDATE */
    protected void performDatabaseAction(String inputQuery) {
        /* Checks if input was empty */
        if(inputQuery.trim().equals("")) {
            showMessageDialog(Alert.AlertType.WARNING, "Warning", "Please specify a query!", "", "");
            return;
        }

        writeToConsole("# QUERY: " + inputQuery + "\n", false);

        /* This is the background task that performs the database */
        SQLService = new Service<Void>() {
              protected Task<Void> createTask() {
                  return new Task<Void>() {
                      protected Void call() throws Exception {
                          // Lock UI access
                          Platform.runLater(() -> {
                              lockControls(true);
                          });

                          try(Connection conn = DriverManager.getConnection(UniversalController.DB_CONNSTR, UniversalController.DB_CONNUSR, UniversalController.DB_CONNPW);
                              Statement stmt = conn.createStatement()) {

                              boolean isQuery = stmt.execute(inputQuery);

                              Platform.runLater(() -> {
                                  c_lHistory.getItems().add(0, inputQuery);
                              });

                              // If the inputQuery was executed as a query and returns a ResultSet
                              if(isQuery) {

                                  // Clear output table
                                  Platform.runLater(() -> {
                                      c_tOutputTable.getItems().clear();
                                      c_tOutputTable.getColumns().clear();
                                  });

                                  /* Process results */
                                  try(ResultSet rs = stmt.getResultSet()) {
                                      ResultSetMetaData rsmt = rs.getMetaData();
                                      for(int i = 0; i < rsmt.getColumnCount(); i++) {
                                          final int j = i;
                                          TableColumn tc = new TableColumn<>(rsmt.getColumnName(i+1));
                                          // HAX to get this stuff working:
                                          tc.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ObservableList,String>,ObservableValue<String>>(){
                                              public ObservableValue<String> call(TableColumn.CellDataFeatures<ObservableList, String> param) {
                                                  return new SimpleStringProperty(param.getValue().get(j).toString());
                                              }

                                          });
                                          Platform.runLater(() -> {
                                              c_tOutputTable.getColumns().add(tc);
                                          });
                                      }

                                      while(rs.next()) {

                                          if(isCancelled()) {
                                              break;
                                          }

                                          ObservableList<String> rsList = FXCollections.observableArrayList();
                                          for(int i = 0; i < rsmt.getColumnCount(); i++) {
                                              rsList.add(rs.getObject(i+1).toString());
                                          }
                                          Platform.runLater(() -> {
                                              c_tOutputTable.getItems().add(FXCollections.observableArrayList(rsList));
                                          });
                                      }
                                  }



                              } else { // insert and such
                                  int affectedRows = stmt.getUpdateCount();
                                  Platform.runLater(() -> {
                                      writeToConsole("Upated " + affectedRows + " row(s).\n", true);
                                  });


                              }

                          } catch(SQLException exc) {
                              Platform.runLater(() -> {
                                  exc.printStackTrace(UniversalController.exception_pw);
                                  showMessageDialog(
                                          Alert.AlertType.ERROR,
                                          "SQL Exception",
                                          "An SQL error occurred :\n" + exc.getMessage(),
                                          "The StackTrace is the following:",
                                          UniversalController.exception_sw.toString());
                                  //exc.printStackTrace();
                              });
                          }

                          // Unlock UI access
                          Platform.runLater(() -> {
                              lockControls(false);
                          });

                          return null;
                      }
                  };
              }
        };

        SQLService.setOnCancelled(event -> {
            //lockControls(false);
            writeToConsole("# QUERY CANCELLED.\n", false);
        });


        // Start the operations
        SQLService.restart();
    }

    /** Locks or unlocks editing controls and most menus of the GUI
     * @param lock Whether to lock (true) or unlock (false) the editor controls of the UI */
    private void lockControls(boolean lock) {
        c_bPerform.setDisable(lock);
        //pi_Working.setVisible(lock);

        c_bPerform.setVisible(!lock);
        //pi_Working.setVisible(lock);
        c_bCancel.setVisible(lock);

        c_tInput.setEditable(!lock); // when lock = true -> setEditable(false)

        c_lHistoryContext_Execute.setDisable(lock);
        c_lHistoryContext_CopyOver.setDisable(lock);

        menu_Close.setDisable(lock);
        menu_Disconnect.setDisable(lock);
        menu_DatabaseInformation.setDisable(lock);

    }

    /** Displays a message in the c_tOutputLogs TextArea. Optionally focuses on its tab, too.
     * @param message The message to append to the TextArea displaying connection logs
     * @param focusOnTab Whether to switch to the tab containing the c_tOutputLogs TextArea. */
    private void writeToConsole(String message, boolean focusOnTab) {
        c_tOutputLogs.appendText(message);

        if(focusOnTab)
            tp_Main_selectionModel.select(1);
    }

    /** Closes the application when the 'Close' menu is clicked. */
    @FXML protected void menuClick_Close(ActionEvent e) {
        Platform.exit();
    }

    /** Returns to the login window when the 'Disconnect' menu is clicked. */
    @FXML protected void menuClick_Disconnect(ActionEvent e) {
        try {
            Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("Main/GUI/JFX/LoginWindow.fxml"));
            Stage st = new Stage();
            st.setTitle("Univeral Database Manager - Login");
            st.setScene(new Scene(root, 800, 600));
            st.show();

            c_lHistory.getScene().getWindow().hide();
        } catch(IOException exc) {
            exc.printStackTrace(UniversalController.exception_pw);
            showMessageDialog(Alert.AlertType.ERROR,
                "Internal error",
                "Failed to load login window.",
                "The stacktrace was the following:",
                UniversalController.exception_sw.toString());
            //exc.printStackTrace();
        }
    }

    /** Opens an 'About' window when the 'About' menu is clicked. */
    @FXML protected void menuClick_About(ActionEvent e) {

        StringBuilder driverList = new StringBuilder("");
        for(DriverModel m : UniversalController.driverPool) {
            driverList.append(m.getName());
            driverList.append(" @ ");
            driverList.append(m.getDriverClass());
            driverList.append("\n");
        }

        driverList.append("(");
        driverList.append(UniversalController.driverPool.size());
        driverList.append(" drivers are in use)\n");

        showMessageDialog(Alert.AlertType.INFORMATION, "About", "Created by Imre Gera (GEIXAAT.SZE)",
                "List of drivers used:",
                driverList.toString());
    }

    /** Opens the Database Information window when the 'Database Information' menu is clicked. */
    @FXML protected void menuClick_DatabaseInformation(ActionEvent e) {
        try {
            Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("Main/GUI/JFX/DatabaseInfo.fxml"));
            Stage st = new Stage();
            st.setTitle("Database Information");
            st.initModality(Modality.APPLICATION_MODAL);

            st.setScene(new Scene(root, 600, 400));
            st.showAndWait();
        } catch(IOException exc) {
            exc.printStackTrace(UniversalController.exception_pw);
            UniversalController.showMessageDialog(
                    Alert.AlertType.ERROR,
                    "",
                    "Failed to load the Database Information dialog.",
                    "Here is the exception stacktrace:",
                    UniversalController.exception_sw.toString()
            );
            //exc.printStackTrace();
        }
    }


    /** Executes the selected query from the history view when the 'Execute' context menu item is clicked. */
    @FXML protected void historyContext_Execute(ActionEvent e) {
        String selItem = c_lHistory.getSelectionModel().getSelectedItem();
        if(selItem != null) {
            performDatabaseAction(selItem);
        }
    }

    /** Copies the selected query into the editing area when the context menu item is clicked. */
    @FXML protected void historyContext_CopyOver(ActionEvent e) {
        String selItem = c_lHistory.getSelectionModel().getSelectedItem();
        if(selItem != null) {
            c_tInput.setText(selItem);
        }
    }

    /** Deselects the current selected item in the history view when the context menu item is clicked. */
    @FXML protected void historyContext_ClearSelection(ActionEvent e) {
        c_lHistory.getSelectionModel().clearSelection();
    }

    /** Check if there are any selected items in the history view when the context menu is shown and
     * disables the menu items that cannot be used. */
    @FXML protected void contextMenu_Shown(WindowEvent e) {
        if(c_lHistory.getSelectionModel().getSelectedItem() == null) {
            c_lHistoryContext_Execute.setDisable(true);
            c_lHistoryContext_CopyOver.setDisable(true);
        } else {
            c_lHistoryContext_Execute.setDisable(false);
            c_lHistoryContext_CopyOver.setDisable(false);
        }
    }


}
