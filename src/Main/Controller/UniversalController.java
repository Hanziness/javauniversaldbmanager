package Main.Controller;

import Main.Controller.JFX.JFXLoginController;
import Main.Model.DriverModel;
import com.sun.org.apache.xpath.internal.operations.Bool;
import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import java.io.*;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.List;

/**
 * Handles database connectivity and driver management. Also stores login details.
 * This class is responsible for loading the required JDBC drivers, too.
 * @author Imre Gera
 * @see DriverModel
 */
public class UniversalController {

    /* Login service for performing background tasks */
    private static Service<Boolean> loginService;
    private static boolean loginSuccess;

    /* Stores loaded drivers. */
    public static List<DriverModel> driverPool = new ArrayList<DriverModel>();

    /* Exception dialog controls */
    public static Alert messageDialog;
    public static Label md_exceptionLabel;
    public static TextArea md_exceptionTextArea;
    public static GridPane md_exceptionGridPane;
    public static StringWriter exception_sw; // Outputs the contents of exception_pw as String.
    public static PrintWriter exception_pw; // Allows printing into it, can be used with printStackTrace(exception_pw).

    /** Creates the appropriate DriverModels and loads them into the driverPool list
     * Looks for the list in 'res/drivers/DRVLIST.txt' that has the following setup:
     * Driver name (that appears in the list), Jar file path, Connection string with replaceable '#HOST#',
     * Tooltip that is shown in the login window at the host input.
     * @return The list of found drivers, also set in driverPool
     * @see DriverModel*/
    public static List<DriverModel> scanAvailableDrivers() {
        List<DriverModel> drvList = new ArrayList<DriverModel>();

        try {
            FileInputStream driverListFile = new FileInputStream(new File("res/drivers/DRVLIST.txt"));
            BufferedReader driverReader = new BufferedReader(new InputStreamReader(driverListFile));

            String curLine;
            byte rowCounter = 0;
            DriverModel curDriver = null;

            //driverPool.clear();

            while ((curLine = driverReader.readLine()) != null) {
                switch((rowCounter%5)) {
                    case 0: curDriver = new DriverModel();
                    curDriver.setName(curLine);
                    break;
                    case 1: curDriver.setJarPath(curLine); break;
                    case 2: curDriver.setDriverClass(curLine); break;
                    case 3: curDriver.setConnectionStringPattern(curLine); break;
                    case 4: curDriver.setTooltip(curLine);
                    drvList.add(curDriver);
                    System.out.println("# SCAN: Driver " + curDriver.getName() + " found.");
                    break;
                }

                rowCounter += 1;
            }
        } catch(Exception e) {
            System.err.println("# ERROR: Failed to read driver list.");
            e.printStackTrace();
        }

        return drvList;
    }

    /* Connection details */
    private static Connection DB_CONN;
    public static String DB_CONNSTR;
    public static String DB_CONNUSR;
    public static String DB_CONNPW;

    /** Loads driver classes that were put into the driverPool List
     * @return True if every driver was loaded successfully, false otherwise.
     */
    public static boolean loadDrivers(List<DriverModel> drvlist) {
        boolean success = true;

        driverPool.clear();

        for(DriverModel d : drvlist) {
            try {
                File file = new File("res/drivers/" + d.getJarPath() + ".jar");
                URL url = file.toURI().toURL();

                URLClassLoader classLoader = (URLClassLoader)ClassLoader.getSystemClassLoader();
                Method method = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
                method.setAccessible(true);
                method.invoke(classLoader, url);

                Class.forName(d.getDriverClass());
                driverPool.add(d);
                System.out.println("# LOAD: Driver " + d.getName() + " loaded.");
            } catch (Exception e) {
                success = false;
                System.err.println("# ERROR: Failed to load driver for " + d.getName());
                e.printStackTrace();
            }
        }

        return success;
    }

    /** Gets the names of available drivers.
     * @return A list of driver names */
    public static List<String> getAvailableDriverNames() {
        ArrayList<String> drvList = new ArrayList<String>();

        for(int i = 0; i < driverPool.size(); i++) {
            drvList.add(driverPool.get(i).getName());
        }

        return drvList;
    }

    /** Constructor that initializes the exception dialog on the JavaFX Thread and loads the available JDBC drivers. */
    public UniversalController() {
        Platform.runLater(() -> {
            messageDialog = new Alert(Alert.AlertType.NONE);

            md_exceptionLabel = new Label("");
            md_exceptionTextArea = new TextArea("");
            md_exceptionGridPane = new GridPane();

            md_exceptionTextArea.setEditable(false);
            md_exceptionTextArea.setWrapText(true);
            md_exceptionTextArea.setMaxWidth(Double.MAX_VALUE);
            md_exceptionTextArea.setMaxHeight(Double.MAX_VALUE);
            GridPane.setVgrow(md_exceptionTextArea, Priority.ALWAYS);
            GridPane.setHgrow(md_exceptionTextArea, Priority.ALWAYS);


            md_exceptionGridPane.setMaxWidth(Double.MAX_VALUE);
            md_exceptionGridPane.add(md_exceptionLabel, 0, 0);
            md_exceptionGridPane.add(md_exceptionTextArea, 0, 1);

            messageDialog.getDialogPane().setExpandableContent(md_exceptionGridPane);

            exception_sw = new StringWriter();
            exception_pw = new PrintWriter(exception_sw);

        });

        System.out.println("# DEBUG: UniversalController created.");
        List<DriverModel> drvList = scanAvailableDrivers();
        System.out.println("# DEBUG: Driver list scanned.");
        loadDrivers(drvList);
        System.out.println("# DEBUG: Drivers loaded");
    }

    /** Tries to log into the given database and saves the login details for later use.
     * @param driverID An integer denoting the ID of the driver in driverPool
     * @param host The host in appropriate format. The function will replace the driverModel's '#HOST#' with this string.
     * @param username The username used to log into the database.
     * @param pw The password used to log into the database.
     * @return True if connection succeeded, false otherwise. */
    public static boolean performLogin(int driverID, String host, String username, String pw) {
        loginSuccess = false;

        String connStr = driverPool.get(driverID).getConnectionStringPattern();
        try {
            connStr = "jdbc:" + connStr;
            connStr = connStr.replaceFirst("#HOST#", host);
            DB_CONN = DriverManager.getConnection(connStr, username, pw);

            if(DB_CONN != null) {
                DB_CONNSTR = connStr;
                DB_CONNPW = pw;
                DB_CONNUSR = username;
                System.out.println("# LOGIN: Successful login.");

                DB_CONN.close();
                loginSuccess = true;
            }

        } catch(Exception e) {
            JFXLoginController.loginException = e;
        }


        // DEBUG:
        System.out.println("# LOGIN: " + driverID + " @ " + host + " @ " + username + " @ " + pw);
        return loginSuccess;

    }

    /** Shows a message dialog that is capable of displaying exception messages, too
     * @param alertType The type of JavaFX alert that will be shown to the user
     * @param header A short string that will appear as the header of the dialog. Input "" if not needed.
     * @param message The main message field of the dialog. Will always be visible.
     * @param exceptionHeader A short text shown above the exception TextArea. Input "" if not needed.
     * @param exceptionMessage Text for a large TextArea that can be used to show StackTraces. Inputting "" for both exceptionHeader and this will disable the 'Show more details' pane of the dialog. */
    public static void showMessageDialog(Alert.AlertType alertType, String header, String message, String exceptionHeader, String exceptionMessage) {
        messageDialog.setAlertType(alertType);
        messageDialog.setHeaderText(header);
        messageDialog.setContentText(message);
        messageDialog.setTitle(header);

        if(exceptionHeader.equals("") && exceptionMessage.equals("")) {
            messageDialog.getDialogPane().setExpandableContent(null);
        } else {
            messageDialog.getDialogPane().setExpandableContent(md_exceptionGridPane);
        }

        if(!exceptionHeader.equals("")) {
            md_exceptionLabel.setText(exceptionHeader);
            //md_exceptionLabel.setVisible(false);
        } else {
            //md_exceptionLabel.setVisible(true);
        }

        if(!exceptionMessage.equals("")) {
            md_exceptionTextArea.setText(exceptionMessage);
            //md_exceptionTextArea.setVisible(true);
        } else {
            //md_exceptionTextArea.setVisible(false);
        }

        messageDialog.showAndWait();
        UniversalController.exception_sw.getBuffer().setLength(0); // clears the stacktrace store
    }
}
