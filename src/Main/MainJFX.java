package Main;

import Main.Controller.UniversalController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/** Main application class that initializes the database controller and the login window.
 * @author Imre Gera
 * @see UniversalController
 * @see Main.Controller.JFX.JFXLoginController */
public class MainJFX extends Application {

    /* Controller made for database handling */
    private static UniversalController controller;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("GUI/JFX/LoginWindow.fxml"));
        primaryStage.setTitle("Universal Database Manager");
        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.show();
    }


    public static void main(String[] args) {
        controller = new UniversalController();
        launch(args);
    }
}
