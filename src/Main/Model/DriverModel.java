package Main.Model;

public class DriverModel {

    private String name;
    private String jarPath;
    private String driverClass;
    private String connectionStringPattern;
    private String tooltip;

    public String getTooltip() {
        return tooltip;
    }

    public void setTooltip(String tooltip) {
        this.tooltip = tooltip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJarPath() {
        return jarPath;
    }

    public void setJarPath(String jarPath) {
        this.jarPath = jarPath;
    }

    public String getDriverClass() {
        return driverClass;
    }

    public void setDriverClass(String driverClass) {
        this.driverClass = driverClass;
    }

    public String getConnectionStringPattern() {
        return connectionStringPattern;
    }

    public void setConnectionStringPattern(String connectionStringPattern) {
        this.connectionStringPattern = connectionStringPattern;
    }


}
